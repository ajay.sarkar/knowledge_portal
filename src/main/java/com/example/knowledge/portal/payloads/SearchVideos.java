package com.example.knowledge.portal.payloads;

import com.example.knowledge.portal.dto.VideoDto;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;


@Data
@AllArgsConstructor
public class SearchVideos {

    private List<VideoDto> videos;
    private Integer numberOfVideos;
}
