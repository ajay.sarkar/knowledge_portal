package com.example.knowledge.portal.controller;

import com.example.knowledge.portal.dto.UserDto;
import com.example.knowledge.portal.entity.User;
import com.example.knowledge.portal.payloads.ApiResponse;
import com.example.knowledge.portal.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/user/v1")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/addNewUser")
    public ResponseEntity<UserDto> addUser(@RequestBody User user)
    {
        UserDto userDto = this.userService.addUser(user);
        return new ResponseEntity<>(userDto, HttpStatus.CREATED);
    }

    @PostMapping("/updateUser")
    public ResponseEntity<UserDto> updateUser(@RequestBody User user)
    {
        UserDto userDto = this.userService.updateUserDetails(user);
        return new ResponseEntity<>(userDto, HttpStatus.OK);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping("/deleteUser/{userId}")
    public ApiResponse deleteUser(@PathVariable("userId") Long userId)
    {
        this.userService.deleteUser(userId);
        return new ApiResponse("Account deletion ois successful", true);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/allUserDetails")
    public ResponseEntity<List<UserDto>> getAllUserDetails()
    {
        List<UserDto> allUsersDto = this.userService.getAllUsers();
        return new ResponseEntity<>(allUsersDto, HttpStatus.FOUND);
    }

    @PreAuthorize("@userSecurity.hasUserId(authentication, #userId)")
    @GetMapping("/userDetails/{userId}")
    public ResponseEntity<UserDto> getUserById(@PathVariable("userId") Long userId, Authentication authentication)
    {
        UserDto userDto = this.userService.getUserById(userId);
        return new ResponseEntity<>(userDto, HttpStatus.FOUND);
    }
}
