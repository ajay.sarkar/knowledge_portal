package com.example.knowledge.portal.controller;

import com.example.knowledge.portal.dto.CommentDto;
import com.example.knowledge.portal.entity.Comment;
import com.example.knowledge.portal.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@RestController
@RequestMapping("/comment")
public class CommentController {

    @Autowired
    private CommentService commentService;

    @PostMapping("/addComment/{videoId}")
    public ResponseEntity<CommentDto> addComment(@RequestBody Comment comment,
                                                 @PathVariable("videoId") Long videoId,
                                                 Principal principal)
    {
        CommentDto commentDto = this.commentService.addComment(comment, videoId, principal);
        return new ResponseEntity<>(commentDto, HttpStatus.CREATED);
    }
}
