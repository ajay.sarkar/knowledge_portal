package com.example.knowledge.portal.dao;

import com.example.knowledge.portal.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface UserRepo extends JpaRepository<User, Long> {

    @Query("select u from User u where u.email =:email")
    public User getUserByUserName(@Param("email") String email);

}
