package com.example.knowledge.portal.service;

import com.example.knowledge.portal.dao.CommentRepo;
import com.example.knowledge.portal.dao.UserRepo;
import com.example.knowledge.portal.dao.VideoRepo;

import com.example.knowledge.portal.dto.VideoDto;
import com.example.knowledge.portal.entity.Comment;
import com.example.knowledge.portal.entity.User;
import com.example.knowledge.portal.entity.Video;


import com.example.knowledge.portal.exception.ResourceNotFoundException;
import com.example.knowledge.portal.exception.VideoAlreadyExistsException;
import com.example.knowledge.portal.payloads.SearchVideos;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.management.modelmbean.ModelMBean;
import java.io.IOException;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

@Service
public class VideoServiceImpl implements VideoService {
    @Autowired
    private VideoRepo videoRepo;

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private CommentRepo commentRepo;

    @Autowired
    private ModelMapper modelMapper;
        @Override
        public VideoDto getVideo(String name) {
            if(!videoRepo.existsByName(name)){
                throw new ResourceNotFoundException("Video", "Video Name", name);
            }
            Video video = videoRepo.findByName(name);
            return this.modelMapper.map(video, VideoDto.class);
        }

        @Override
        public List<String> getAllVideoNames() {
            return videoRepo.getAllEntryNames();
        }

    @Override
    public SearchVideos searchVideos(String keyword) {
        List<Video> allVideos = this.videoRepo.findByCategoryContaining(keyword);
        List<VideoDto> allVideoDtos = new ArrayList<>();

        int count = 0;

        for(Video video : allVideos)
        {
            allVideoDtos.add(this.modelMapper.map(video, VideoDto.class));
            count++;
        }

        return new SearchVideos(allVideoDtos, count);
    }

    @Override
        public VideoDto saveVideo(MultipartFile file, String name, Principal principal) throws IOException {
           if(this.videoRepo.existsByName(name)){
               throw new VideoAlreadyExistsException();
           }
            //byte[] videoData = file.getBytes();
            Video video = new Video(name,file.getBytes()) ;
            User user = this.userRepo.getUserByUserName(principal.getName());
            video.setUser(user);
            Video newVideo = this.videoRepo.save(video);
            return this.modelMapper.map(newVideo, VideoDto.class);
        }
    }

